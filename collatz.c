#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int ChildProcess( int x);
void ParentProcess(int count);

int main (int argc, char** argv){

pid_t pid;
pid = fork();
int x = atoi(argv[1]);

int count;


if (pid == 0){

	while(x != 1){
	printf("%d\n", x);
	x = ChildProcess(x);
	}
	
	printf("1\n");
}
else {
	ParentProcess(count);
	}
	return 0;
}
	
int ChildProcess(int x){


if (x % 2 == 0 ) {
	x = x/2;
	return x;
	}
	
else {
	x = (x*3)+1;
	return x;
	}
}
	
void ParentProcess(int count){

 printf("Start\n");
 
 wait(NULL);
 
 printf("End\n");
 
}
